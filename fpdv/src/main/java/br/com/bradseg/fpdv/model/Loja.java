package br.com.bradseg.fpdv.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;

@Entity
public class Loja {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String nomeLoja;
	private String regional;
	private String parceiro;
	private String diretoria;
	private String email;
	private String telefone;
	private String celular;
	private String cep;
	private String endereco;
	private int numero;
	private String indicadorIsAtivo;
	private String bairro;
	private String cidade;
	private String estado;
	
	@Transient
	private List<String> selectReg = new ArrayList<String>();
	
	@Transient
	private List<String> selectDir = new ArrayList<String>();
	

	public Loja() {}
	
	public void setId(Long id){
		this.id = id;
	}
	
	public Long getId(){
		return id;
	}


	public List<String> getSelectReg() {
		return selectReg;
	}

	public void setSelectReg(List<String> selectReg) {
		this.selectReg = selectReg;
	}

	public List<String> getSelectDir() {
		return selectDir;
	}

	public void setSelectDir(List<String> selectDir) {
		this.selectDir = selectDir;
	}
	public String getNomeLoja() {
		return nomeLoja;
	}
	public void setNomeLoja(String nomeLoja) {
		this.nomeLoja = nomeLoja;
	}
	public String getRegional() {
		return regional;
	}
	public void setRegional(String regional) {
		this.regional = regional;
	}
	public String getParceiro() {
		return parceiro;
	}
	public void setParceiro(String parceiro) {
		this.parceiro = parceiro;
	}
	public String getDiretoria() {
		return diretoria;
	}
	public void setDiretoria(String diretoria) {
		this.diretoria = diretoria; 	 		
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public String getCelular() {
		return celular;
	}
	public void setCelular(String celular) {
		this.celular = celular;
	}
	public String getCep() {
		return cep;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	public String getEndereco() {
		return endereco;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	public int getNumero() {
		return numero;
	}
	public void setNumero(int numero) {
		this.numero = numero;
	}
	public String getBairro() {
		return bairro;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public String getCidade() {
		return cidade;
	}
	public void setCidade(String cidade) {
		this.cidade = cidade;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getIndicadorIsAtivo() {
		return indicadorIsAtivo;
	}

	public void setIndicadorIsAtivo(String indicadorIsAtivo) {
		this.indicadorIsAtivo = indicadorIsAtivo;
	}

}
