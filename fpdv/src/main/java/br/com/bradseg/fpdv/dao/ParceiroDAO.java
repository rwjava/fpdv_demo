package br.com.bradseg.fpdv.dao;

import java.util.List;

import javax.persistence.EntityManager;

import br.com.bradseg.fpdv.connection.ConnectionFactory;
import br.com.bradseg.fpdv.model.Parceiro;

public class ParceiroDAO implements DAO<Parceiro> {

	private EntityManager entityManager = ConnectionFactory.getConnection();

	@Override
	public void insert(Parceiro p) {
		entityManager.persist(p);
	}

	@Override
	public void remove(Long id, Parceiro p) {
		Parceiro result = get(id);
		entityManager.remove(result);
	}

	@Override
	public Parceiro get(Long id) {
		return entityManager.find(Parceiro.class, id);
	}

	@Override
	public List<Parceiro> list() {
		return entityManager.createQuery("from Parceiro").getResultList();
	}

	@Override
	public void update(Long id, Parceiro p) {
		entityManager.merge(p);
	}
}
