package br.com.bradseg.fpdv.dao;

import java.util.List;

import javax.persistence.EntityManager;

import br.com.bradseg.fpdv.connection.ConnectionFactory;
import br.com.bradseg.fpdv.model.Produto;

public class ProdutoDAO implements DAO<Produto>{

	
	private EntityManager entityManager = ConnectionFactory.getConnection();
	
	@Override
	public void insert(Produto p) {
		entityManager.persist(p);
	}

	@Override
	public void remove(Long id, Produto p) {
		Produto result = get(id);
		entityManager.remove(result);
	}

	@Override
	public Produto get(Long id) {
		return entityManager.find(Produto.class, id);
	}

	@Override
	public List<Produto> list(){
		return entityManager.createQuery("from Produto").getResultList();
	}

	@Override
	public void update(Long id, Produto t){
		entityManager.merge(t);
	}

}
