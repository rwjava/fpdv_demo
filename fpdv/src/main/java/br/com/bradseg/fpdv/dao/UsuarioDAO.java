package br.com.bradseg.fpdv.dao;

import java.util.List;

import javax.persistence.EntityManager;

import br.com.bradseg.fpdv.connection.ConnectionFactory;
import br.com.bradseg.fpdv.model.Usuario;

public class UsuarioDAO implements DAO<Usuario>{
	
	private EntityManager entityManager = ConnectionFactory.getConnection();
	
	@Override
	public void insert(Usuario u){
		entityManager.persist(u);
	}
	
	@Override
	public void remove(Long id, Usuario u){
		Usuario result = get(id);
		entityManager.remove(result);
	}
	
	@Override
	public Usuario get(Long id) {
		return entityManager.find(Usuario.class, id);
	}
	
	@Override
	public List<Usuario> list(){
		return entityManager.createQuery("from Usuario").getResultList();
	}
	
	@Override
	public void update(Long id, Usuario u){
		entityManager.merge(u);
	}
}
