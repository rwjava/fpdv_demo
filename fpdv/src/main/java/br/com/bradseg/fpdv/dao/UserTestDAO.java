package br.com.bradseg.fpdv.dao;

import java.util.List;

import javax.persistence.EntityManager;

import br.com.bradseg.fpdv.connection.ConnectionFactory;
import br.com.bradseg.fpdv.model.UserTest;


public class UserTestDAO implements DAO<UserTest>{
	
	private EntityManager entityManager = ConnectionFactory.getConnection();

	@Override
	public void insert(UserTest t) {
		entityManager.persist(t);
	}

	@Override
	public void remove(Long id, UserTest t) {
		UserTest result = get(id);
		entityManager.remove(result);
	}

	@Override
	public UserTest get(Long id) {
		return entityManager.find(UserTest.class, id);
	}

	@Override
	public List<UserTest> list() {
		return entityManager.createQuery("from UserTest").getResultList();
	}

	@Override
	public void update(Long id, UserTest t) {
		// TODO Auto-generated method stub
		
	}

}
