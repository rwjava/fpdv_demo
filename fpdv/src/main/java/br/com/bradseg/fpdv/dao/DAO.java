package br.com.bradseg.fpdv.dao;

import java.util.List;

public interface DAO<T> {
	
	public void insert(T t);
	public void remove(Long id, T t);
	public T get(Long id);
	public List<T> list();
	public void update(Long id, T t);

}
