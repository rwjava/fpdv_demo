package br.com.bradseg.fpdv.bean;

import javax.faces.bean.ManagedBean;

import org.apache.log4j.Logger;

import br.com.bradseg.fpdv.dao.UserTestDAO;
import br.com.bradseg.fpdv.model.UserTest;

@ManagedBean
public class UserBean {
	
	public UserBean() {
		logger.info("pegando usuario");
		
		getUser();
	}
	
	private UserTestDAO userTestDAO  = new UserTestDAO();
	private static Logger logger = Logger.getLogger(UserBean.class);
	UserTest user = new UserTest();
	
	private UserTest userTest = new UserTest();

	public UserTest getUserTest() {
		return userTest;
	}

	public void setUserTest(UserTest userTest) {
		this.userTest = userTest;
	}
	
	
	private void getUser(){
		userTest = userTestDAO.get(1L);
	}
	
	public void save(){
		user.setDetails("User Test");
		user.setName("Ebix Consulting");
		userTestDAO.insert(user);
	}

}
