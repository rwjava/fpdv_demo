package br.com.bradseg.fpdv.dao;

import java.util.List;

import javax.persistence.EntityManager;

import br.com.bradseg.fpdv.connection.ConnectionFactory;
import br.com.bradseg.fpdv.model.Loja;

public class LojaDAO implements DAO<Loja> {
	
	private EntityManager entityManager = ConnectionFactory.getConnection();

	@Override
	public void insert(Loja l) {
		entityManager.persist(l);
		
	}

	@Override
	public void remove(Long id, Loja l) {
		Loja result = get(id);
		entityManager.remove(result);
	}

	@Override
	public Loja get(Long id) {
		return entityManager.find(Loja.class, id);
	}

	@Override
	public List<Loja> list() {
		return entityManager.createQuery("from Loja").getResultList();
	}

	@Override
	public void update(Long id, Loja l) {
		entityManager.merge(l);
		
	}

}
