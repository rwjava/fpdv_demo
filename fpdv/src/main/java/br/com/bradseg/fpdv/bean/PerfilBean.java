package br.com.bradseg.fpdv.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.view.ViewScoped;

import org.apache.log4j.Logger;

import br.com.bradseg.fpdv.dao.PerfilDAO;
import br.com.bradseg.fpdv.model.Perfil;

@ManagedBean(name="perfilBean")
@ViewScoped
public class PerfilBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public PerfilBean() {
		logger.info("Pegando Perfil");
		
	}
	
	private PerfilDAO perfilDAO = new PerfilDAO();
	private Perfil perfil = new Perfil();
	private static Logger logger = Logger.getLogger(PerfilBean.class);
	private List<Perfil> perfis;
	private Long idBuscaPerfil;
	private String messageStatus = "";
	private List<String>selectIndicador = new ArrayList<String>();
	

	public List<Perfil> getPerfis() {
		return perfilDAO.list();
	}
	public void setPerfis(List<Perfil> perfis) {
		this.perfis = perfis;
	}

	public Long getIdBuscaPerfil() {
		return idBuscaPerfil;
	}
	
	public void setIdBuscaPerfil(Long idBuscaPerfil) {
		this.idBuscaPerfil = idBuscaPerfil;
	}
	
	public Perfil getPerfil() {
		return perfil;
	}
	
	public String getMessageStatus() {
		return messageStatus;
	}

	public PerfilDAO getPerfilDAO() {
		return perfilDAO;
	}
	
	
	public void indicadores() {

		selectIndicador.add("Ativo");
		selectIndicador.add("Inativo");
	
	}
	
	public List<String> getSelectIndicador() {
		return selectIndicador;
	}
	
		//INSERIR
	public void gravarPerfil() {
		if(perfil != null) {
			perfilDAO.insert(perfil);
			messageStatus="Perfil cadastrado com sucesso!";
		}else
			messageStatus="Erro ao cadastrar o Perfil " + perfil.getNmPerfil();
		
	}
	
	// CONSULTAR
	public String buscarPerfil() {
		if (idBuscaPerfil != null && idBuscaPerfil > 0) {
			this.perfil = perfilDAO.get(idBuscaPerfil);
			return "/pages/perfil/ConsultarPerfil";
		} else
			return null;
	}
	
	//ALTERAR
	public String buscarPerfilAlterar() {
		if (idBuscaPerfil != null && idBuscaPerfil > 0) {
			this.perfil = perfilDAO.get(idBuscaPerfil);
			return "/pages/perfil/AlterarPerfil";
		} else
			return null;
	}
	
	public String alterarPerfil() {
		try {
			perfilDAO.update(null, perfil);
			messageStatus  = "Dados alterados com sucesso";
		}catch (Exception e) {
			messageStatus = "Erro ao alterar os dados do perfil: " + this.perfil.getNmPerfil();
			e.printStackTrace();
		}
		return "/pages/perfil/BuscarPerfilAlterar";
	}
	
	//Excluir
		public String buscarPerfilExcluir() {
			if (idBuscaPerfil != null && idBuscaPerfil > 0) {
				this.perfil = perfilDAO.get(idBuscaPerfil);
				return "/pages/perfil/ExcluirPerfil";
			} else
				return null;
		}
		
		public String excluirPerfil(Long idPerfil) {
			try {
				perfilDAO.remove(perfil.getId(), null);
				messageStatus  = "Perfil excluido com sucesso";
			
			}catch (Exception e) {
				messageStatus = "Erro ao excluir perfil: " + this.perfil.getNmPerfil();
				e.printStackTrace();
			}
			return "/pages/perfil/buscarPerfilExcluir";
		}
		
	}
	


	


