package br.com.bradseg.fpdv.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.view.ViewScoped;

import org.apache.log4j.Logger;

import br.com.bradseg.fpdv.dao.ProdutoDAO;
import br.com.bradseg.fpdv.model.Produto;

@ManagedBean(name = "cadastrarProdutoBean")
@ViewScoped
public class CadastrarProdutoBean implements Serializable {

	private static final long serialVersionUID = 1L;

	public CadastrarProdutoBean() {
		logger.info("pegando produto");
		indicadores();
	}

	private ProdutoDAO produtoDAO = new ProdutoDAO();
	private static Logger logger = Logger.getLogger(CadastrarProdutoBean.class);
	private Produto produto = new Produto();
	private List<Produto> produtos;
	private Long idProduto;
	private String messageStatus = "";
	private List<String> selectIndicador = new ArrayList<String>();
	private List<String> selectData = new ArrayList<String>();

	public Long getIdProduto() {
		return idProduto;
	}

	public void setIdProduto(Long idProduto) {
		this.idProduto = idProduto;
	}

	public ProdutoDAO getProdutoDAO() {
		return produtoDAO;
	}

	public String setMessageStatus() {
		return messageStatus;
	}

	public List<Produto> getProdutos() {
		return produtoDAO.list();
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public void indicadores() {
		selectIndicador.add("Ativo");
		selectIndicador.add("Inativo");
		
		datas();
	}

	public void datas() {

		selectData.add("10/06/2019");
		selectData.add("11/06/2019");
		selectData.add("12/06/2019");
		selectData.add("13/06/2019");
		selectData.add("14/06/2019");

	}

	public void setProdutos(List<Produto> produtos) {
		this.produtos = produtos;
	}

	public String getMessageStatus() {
		return messageStatus;
	}
	
	public List<String> getSelectIndicador() {
		return selectIndicador;
	}

	public void setSelectIndicador(List<String> selectIndicador) {
		this.selectIndicador = selectIndicador;
	}

	public List<String> getSelectData() {
		return selectData;
	}

	public void setSelectData(List<String> selectData) {
		this.selectData = selectData;
	}
	
	public String gravarProduto() {
		if (produto != null) {
			produtoDAO.insert(produto);
			messageStatus="Produto cadastrado com sucesso!";
		}else
			messageStatus="Erro ao cadastrar o produto "+ produto.getNomeProd();
		return "/pages/produto/cadastrar-produto";
	} 

	public String buscaProduto() {
		if (idProduto != null && idProduto > 0) {
			this.produto = produtoDAO.get(idProduto);
			return "/pages/produto/consulta-produto";
		} else
			return null;
	}

	public String buscaProdutoExcluir() {
		if (idProduto != null && idProduto > 0){
			this.produto = produtoDAO.get(idProduto);
			return "/pages/produto/excluir-produto";
		} else
			return null;
	}
	
	public String buscaProdutoAlterar() {
		if (idProduto != null && idProduto > 0) {
			this.produto = produtoDAO.get(idProduto);
			return "/pages/produto/alterar-produto";
		} else
			return null;
	}

	public String consultaProduto() {
		if (idProduto != null && idProduto > 0) {
			this.produto = produtoDAO.get(idProduto);
			return "/pages/produto/consulta-produto";
		} else
			return null;
	}

	public String excluirProduto(Long idProduto) {
		try {
			produtoDAO.remove(produto.getId(), null);
			messageStatus = "Produto excluido com sucesso: " + this.produto.getNomeProd();
		} catch (Exception e){
			messageStatus = "Erro ao excluir produto: " + this.produto.getNomeProd();
		}
		
		return "/pages/produto/buscar-excluir-produto";
	}
	
	public String alterarProduto(Long idProduto) {
		try {
			produtoDAO.update(null,produto);
			messageStatus = "Produto alterado com sucesso: " + this.produto.getNomeProd();
		} catch (Exception e){
			messageStatus = "Erro ao alterado produto: " + this.produto.getNomeProd();
		}
		
		return "/pages/produto/consulta-produto";
	}


}
