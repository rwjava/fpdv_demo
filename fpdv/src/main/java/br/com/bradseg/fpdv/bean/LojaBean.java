package br.com.bradseg.fpdv.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.view.ViewScoped;

import org.apache.log4j.Logger;

import br.com.bradseg.fpdv.dao.LojaDAO;
import br.com.bradseg.fpdv.model.Loja;

@ManagedBean(name = "lojaBean")
@ViewScoped
public class LojaBean implements Serializable {

	private static final long serialVersionUID = 1L;

	public LojaBean() {
		logger.info("pegando loja");
		regionais();
	}
	
	//*********Atributos***************\\
	private LojaDAO lojaDAO = new LojaDAO();
	private static Logger logger = Logger.getLogger(LojaBean.class);
	private Loja loja = new Loja();
	private List<Loja> lojas;
	private Long idBuscaLoja;
	private String messageStatus = "";
	private List<String> selectReg = new ArrayList<String>();	
	private List<String> selectDir = new ArrayList<String>();

	public Loja getLoja() {
		return loja;
	}

	public void regionais() {
		selectReg.add("Regional 1");
		selectReg.add("Regional 2");
		selectReg.add("Regional 3");
		selectReg.add("Regional 4");

		diretorias();

	}

	public void diretorias() {
		selectDir.add("Diretoria 1");
		selectDir.add("Diretoria 2");
		selectDir.add("Diretoria 3");
		selectDir.add("Diretoria 4");

	}
	
	 // Getter and Setter
	
	public List<String> getSelectDir() {
		return selectDir;
	}
	
	public List<String> getSelectReg() {
		return selectReg;
	}

	public List<Loja> getLojas() {
		return lojaDAO.list();
	}

	public void setLojas(List<Loja> lojas) {
		this.lojas = lojas;
	}

	public Long getIdBuscaLoja() {
		return idBuscaLoja;
	}

	public void setIdBuscaLoja(Long idBuscaLoja) {
		this.idBuscaLoja = idBuscaLoja;
	}

	public String getMessageStatus() {
		return messageStatus;
	}
	
	
	//*********Metodos***************\\
	public void gravarLoja() {
		if(loja!= null) {
			lojaDAO.insert(loja);
			messageStatus = "Loja cadastrada com sucesso!";
		}else		
			messageStatus = "Erro ao cadastrar a loja "+loja.getNomeLoja();
	}

	public String buscaLoja() {
		if (idBuscaLoja != null && idBuscaLoja > 0) {
			this.loja = lojaDAO.get(idBuscaLoja);
			return "/pages/loja/consultarLoja";
		} else
			return null;
	}

	public String buscaLojaExcluir() {
		if (idBuscaLoja != null && idBuscaLoja > 0) {
			this.loja = lojaDAO.get(idBuscaLoja);
			return "/pages/loja/excluirLoja";
		} else
			return null;
	}

	public String excluirLoja(Long idLoja) {
		try {
			lojaDAO.remove(loja.getId(), null);
			messageStatus = "Loja excluida com sucesso!";
		} catch (Exception e) {
			messageStatus = "Erro ao excluir a loja: " + this.loja.getNomeLoja();
		}

		return "/pages/loja/buscaLojaExcluir";
	}

	public String buscaLojaAlterar() {
		if (idBuscaLoja != null && idBuscaLoja > 0) {
			this.loja = lojaDAO.get(idBuscaLoja);
			return "/pages/loja/alterarLoja";
		} else
			return null;
	}

	public String alterarLoja() {
		try {	
			lojaDAO.update(null, loja);
			
			messageStatus = "Dados alterados com sucesso";
		} catch (Exception e) {
			messageStatus = "Erro ao alterar a loja: " + this.loja.getNomeLoja();
			e.printStackTrace();
		}
		return "/pages/loja/buscarLojaAlterar";
	}
	

}