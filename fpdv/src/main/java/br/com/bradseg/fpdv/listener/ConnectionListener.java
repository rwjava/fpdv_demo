package br.com.bradseg.fpdv.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import br.com.bradseg.fpdv.connection.ConnectionFactory;

@WebListener
public class ConnectionListener implements ServletContextListener {

    public ConnectionListener() { }

	public void contextDestroyed(ServletContextEvent arg0)  { 
		ConnectionFactory.stop();
	}

    public void contextInitialized(ServletContextEvent arg0)  { 
    	ConnectionFactory.start();
    }
	
}
