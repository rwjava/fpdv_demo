package br.com.bradseg.fpdv.model;



import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity
public class Produto {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String nomeProd;
	private String descProd;
	private String indicador;
	private String dataDisponibilidade;
	
	public Produto() {
	}
	
	public String getNomeProd() {
		return nomeProd;
	}
	
	public String getDescProd() {
		return descProd;
	}
	public void setDescProd(String descProd) {
		this.descProd = descProd;
	}
	public String getIndicador() {
		return indicador;
	}
	public void setIndicador(String indicador) {
		this.indicador = indicador;
	}
	public String getDataDisponibilidade() {
		return dataDisponibilidade;
	}
	public void setDataDisponibilidade(String dataDisponibilidade) {
		this.dataDisponibilidade = dataDisponibilidade;
	}
	public void setNomeProd(String nomeProd) {
		this.nomeProd = nomeProd;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}	
}
