package br.com.bradseg.fpdv.dao;

import java.util.List;

import javax.persistence.EntityManager;

import br.com.bradseg.fpdv.connection.ConnectionFactory;
import br.com.bradseg.fpdv.model.Perfil;

public class PerfilDAO implements DAO<Perfil> {

	private EntityManager entityManager = ConnectionFactory.getConnection();

	@Override
	public void insert(Perfil perfil) {
		entityManager.persist(perfil);

	}

	@Override
	public void remove(Long id, Perfil perfil) {
		Perfil result = get(id);
		entityManager.remove(result);

	}

	@Override
	public Perfil get(Long id) {
		return entityManager.find(Perfil.class, id);
	}

	@Override
	public List<Perfil> list() {
		return entityManager.createQuery("from Perfil").getResultList();
	}

	@Override
	public void update(Long id, Perfil perfil) {
		entityManager.merge(perfil);

	}
}
