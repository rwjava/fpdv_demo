package br.com.bradseg.fpdv.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.view.ViewScoped;

import org.apache.log4j.Logger;

import br.com.bradseg.fpdv.dao.ParceiroDAO;
import br.com.bradseg.fpdv.model.Parceiro;

@ManagedBean(name = "parceiroBean")
@ViewScoped
public class ParceiroBean implements Serializable {

	private static final long serialVersionUID = 1L;

	public ParceiroBean() {
		logger.info("Pegando parceiro");
		segmentos();
	}

	private ParceiroDAO parceiroDAO = new ParceiroDAO();
	private static Logger logger = Logger.getLogger(ParceiroBean.class);
	private Parceiro parceiro = new Parceiro();
	private List<Parceiro> parceiros;
	private Long idBuscaParceiro;
	private String messageStatus = "";
	private List<String> selectSeg = new ArrayList<String>();

	public void segmentos() {
		selectSeg.add("Segmento 1");
		selectSeg.add("Segmento 2");
		selectSeg.add("Segmento 3");
		selectSeg.add("Segmento 4");
	}
	
	// ******METODOS********

	// Gravar
	public void gravarParceiros() {
		if(parceiro != null) {
			parceiroDAO.insert(parceiro);
			messageStatus = "Parceiro Cadastrado com Sucesso.";
		}else
			messageStatus = "Erro ao cadastrar o Parceiro "+ parceiro.getNmParceiro();
	}

	// Consultar
	public String buscarParceiros() {
		if (idBuscaParceiro != null && idBuscaParceiro > 0) {
			this.parceiro = parceiroDAO.get(idBuscaParceiro);
			return "/pages/parceiro/consultar-parceiro";
		} else
			return null;
	}

	// Excluir

	public String buscarParceiroExcluir() {
		if (idBuscaParceiro != null && idBuscaParceiro > 0) {
			this.parceiro = parceiroDAO.get(idBuscaParceiro);
			return "/pages/parceiro/excluir-parceiro";
		} else
			return null;
	}

	public String excluirParceiro(Long idParceiro) {
		try {
			parceiroDAO.remove(parceiro.getId(), null);
			messageStatus = "Parceiro excluido com sucesso!";
		} catch (Exception e) {
			messageStatus = "Erro ao excluir o Parceiro: " + this.parceiro.getNmParceiro();
		}
		return "/pages/parceiro/buscarParceiroExcluir";
	}
	
	//Alterar
	
	public String buscarParceiroAlterar() {
		if (idBuscaParceiro != null && idBuscaParceiro > 0) {
			this.parceiro = parceiroDAO.get(idBuscaParceiro);
			return "/pages/parceiro/alteracao-parceiro";
		} else
			return null;
	}

	public String alterarParceiro() {
		try {
			parceiroDAO.update(null, parceiro);
			messageStatus = "Dados alterados com sucesso";
		
		}catch (Exception e) {
			messageStatus = "Erro ao alterar os dados do parceiro: " + this.parceiro.getNmParceiro();
			e.printStackTrace();
		}
		return "/pages/parceiro/buscarParceiroAlterar";
	}
	
	// *********GETTERS AND SETTERS*************
	public Parceiro getParceiro() {
		return parceiro;
	}

	public List<Parceiro> getParceiros() {
		return parceiroDAO.list();
	}

	public void setParceiros(List<Parceiro> parceiros) {
		this.parceiros = parceiros;
	}

	public Long getIdBuscaParceiro() {
		return idBuscaParceiro;
	}

	public void setIdBuscaParceiro(Long idBuscaParceiro) {
		this.idBuscaParceiro = idBuscaParceiro;
	}

	public String getMessageStatus() {
		return messageStatus;
	}

	public List<String> getSelectSeg() {
		return selectSeg;
	}

}
