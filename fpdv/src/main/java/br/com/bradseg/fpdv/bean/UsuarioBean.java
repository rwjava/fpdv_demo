package br.com.bradseg.fpdv.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.view.ViewScoped;

import org.apache.log4j.Logger;

import br.com.bradseg.fpdv.dao.UsuarioDAO;
import br.com.bradseg.fpdv.model.Usuario;

@ManagedBean(name = "usuarioBean")
@ViewScoped
public class UsuarioBean implements Serializable {

	private static final long serialVersionUID = 1L;

	public UsuarioBean() {
		logger.info("pegando usuario");
		indicadores();
	}

	private UsuarioDAO usuarioDAO = new UsuarioDAO();
	private static Logger logger = Logger.getLogger(UsuarioBean.class);
	private Usuario usuario = new Usuario();
	private List<Usuario> usuarios;
	private Long idUsuario;
	private String messageStatus = "";
	private List<String> selectIndicador = new ArrayList<String>();
	private List<String> selectData = new ArrayList<String>();

	public UsuarioDAO getUsuarioDao() {
		return usuarioDAO;
	}

	public void setUsuarioDao(UsuarioDAO usuarioDao) {
		this.usuarioDAO = usuarioDao;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public String getMessageStatus() {
		return messageStatus;
	}

	public void setMessageStatus(String messageStatus) {
		this.messageStatus = messageStatus;
	}

	public Long getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Long idUsuario) {
		this.idUsuario = idUsuario;
	}
	
	public List<Usuario> getUsuarios() {
		return usuarioDAO.list();
	}

	public void setUsuarios(List<Usuario> usuarios) {
		this.usuarios = usuarios;
	}
	
	public void indicadores() {

		selectIndicador.add("Ativo");
		selectIndicador.add("Inativo");

		datas();
	}
	
	public void datas() {

		selectData.add("28/07/2019");
		selectData.add("29/07/2019");
		selectData.add("30/07/2019");
		selectData.add("31/07/2019");
	}
	
	public List<String> getSelectIndicador() {
		return selectIndicador;
	}

	public List<String> getSelectData() {
		return selectData;
	}
	
	public String gravarUsuario(){
		if (usuario != null) {
			usuarioDAO.insert(usuario);
			messageStatus = "Usuário "+ usuario.getNmUsuario()+ "cadastrado com Sucesso. ";
		}else
			messageStatus = "Erro ao cadastrar Usuário " + usuario.getNmUsuario();
		return "/pages/usuario/cadastrar-usuario";
	}
	
	public String buscarUsuario(){
		if (idUsuario != null && idUsuario > 0){
			this.usuario = usuarioDAO.get(idUsuario);
			return "/pages/usuario/consulta-usuario";
		} else
			return null;
	}
	
	public String buscaUsuarioExcluir(){
		if (idUsuario != null && idUsuario > 0){
			this.usuario = usuarioDAO.get(idUsuario);
			return "/pages/usuario/excluir-usuario";
		} else
			return null;
	}
	
	public String buscaUsuarioAlterar(){
		if (idUsuario != null && idUsuario > 0){
			this.usuario = usuarioDAO.get(idUsuario);
			return "/pages/usuario/alterar-usuario";
		} else
			return null;
	}
	
	public String consultaUsuario(){
		if(idUsuario != null && idUsuario > 0){
			this.usuario = usuarioDAO.get(idUsuario);
			return "/pages/usuario/consulta-usuario";
		} else
			return null;
	}
	
	public String excluirUsuario(Long idUsuario){
		try {
			usuarioDAO.remove(usuario.getId(), null);
			messageStatus = "Usuario excluido com sucesso: " + this.usuario.getNmUsuario();	
		} catch (Exception e) {
			messageStatus = "Erro ao excluir usuario: " + this.usuario.getNmUsuario();
		}
			return "/pages/usuario/buscar-excluir-usuario";
	}
	
	public String alterarUsuario(Long idUsuario){
		try {
			usuarioDAO.update(null,usuario);
			messageStatus = "Usuario alterado com sucesso: " + this.usuario.getNmUsuario();
		} catch (Exception e) {
			messageStatus = "Erro ao alterar usuario: " + this.usuario.getNmUsuario();
		}
		
		return "/pages/usuario/buscar-alterar-usuario";
	}

}
