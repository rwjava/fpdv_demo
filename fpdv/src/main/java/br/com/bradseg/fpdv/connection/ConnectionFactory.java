package br.com.bradseg.fpdv.connection;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.apache.log4j.Logger;

public class ConnectionFactory {
	
	private static final ThreadLocal<EntityManager> threadLocal = new ThreadLocal<EntityManager>();
	private static EntityManagerFactory instance;
	
	private static Logger logger = Logger.getLogger(ConnectionFactory.class);
	
	
	public static void start() {
		if (instance == null) {
			synchronized (ConnectionFactory.class) {
				if (instance == null) {
					logger.info("start connection");
					EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("fpdv");
					instance = entityManagerFactory; 
				}
			}
		}
	}
	
	public static void stop() {
		if (instance != null && instance.isOpen()) {
			logger.info("close connection");
			instance.close();
			instance = null;
		}
	}
	
	public static EntityManager getConnection() {
		EntityManager entityManager = threadLocal.get();
		if (entityManager == null || !entityManager.isOpen()) {			
			if (instance == null) {
				start();
			}
			synchronized (ConnectionFactory.class) {
				entityManager = instance.createEntityManager();
				threadLocal.set(entityManager);	
			}
		}
		return entityManager;
	}
}
