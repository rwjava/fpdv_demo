package br.com.bradseg.fpdv.filter;

import java.io.IOException;

import javax.persistence.EntityManager;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import br.com.bradseg.fpdv.connection.ConnectionFactory;

@WebFilter("/*")
public class ConnectionFilter implements Filter {

    public ConnectionFilter() { }

	public void destroy() { }
	
	private static Logger logger = Logger.getLogger(ConnectionFilter.class);

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse resp = (HttpServletResponse) response;
		HttpSession session = req.getSession();
		EntityManager entityManager = ConnectionFactory.getConnection();
		
		try{
			entityManager.getTransaction().begin();
			chain.doFilter(request, response);
			entityManager.getTransaction().commit();

		}catch(Exception e){
			logger.error(e.getMessage());
			if(entityManager.getTransaction().isActive())
				entityManager.getTransaction().rollback();
		}
		finally{
			entityManager.close();
		}
	}

	public void init(FilterConfig fConfig) throws ServletException { }

}
